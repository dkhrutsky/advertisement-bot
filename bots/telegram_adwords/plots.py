import pandas as pd
import matplotlib as mpl
import random
import datetime
import logging
from io import StringIO, BytesIO

# For use in console app
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns

sns.set()


def get_plot(data, labels=['Impressions', 'Clicks', 'Cost']):
    dfs = {k: pd.read_csv(StringIO(data[k]), dtype=object, index_col=0) for k in data.keys()}
    # DEBUG BEGIN
    logging.warning('Using artificial data for plotting.')
    for k, df in dfs.items():
        for label in labels:
            df[label] = pd.Series([random.randint(0, 100) for _ in range(df.index.size)], index=df.index)

    test_kwid = df.index[0]
    values = []
    dates = []
    label = 'Impressions'
    for k, df in dfs.items():
        dates.append(k)
        values.append(df[label][test_kwid])
    # DEBUG END

    x = [datetime.datetime.strptime(d, '%Y%m%d').date() for d in dates]
    y = values
    return plot_date(x, y, label)


def plot_date(x, y, label='Impressions'):
    """
    x - list of dates
    y - values for dates
    label - label for y-axis and plot name

    Returns
    buff: BytesIO - resulting plot in png format
    """
    plt.xlabel('Date')
    plt.ylabel(label)
    plt.title('{label} for period {begin} – {end}'.format(label=label, begin=x[0], end=x[-1]))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.plot(x, y, 'ro')
    plt.plot(x, y, 'r')
    plt.gcf().autofmt_xdate()
    buff = BytesIO()
    plt.gcf().savefig(buff, format='png', dpi=100)
    plt.close()
    buff.seek(0)
    return buff
