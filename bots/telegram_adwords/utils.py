import logging
import csv
from telegram.ext.dispatcher import run_async
from io import StringIO

TIMEOUT = 5


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def format_campaign_message(campaign, report=None):
    campaign_dict = {
        'ID': campaign.id,
        'Status': campaign.status,
        'Serving Status': campaign.servingStatus,
        'Started': campaign.startDate,
        'Ended': campaign.endDate,
        'Budget': campaign.budget,
        'Settings': campaign.settings,
    }
    message = 'Campaign \"{}\"\n\n'.format(campaign.name) + '\n'.join(
        ['**{}**: {}'.format(key, value) for key, value in campaign_dict.items()])
    if report:
        reader = csv.DictReader(StringIO(report))
        for row in reader:
            if row['Campaign ID'] == str(campaign.id):
                report_dict = row
        report_str = '\n'.join(
            ['**{}**: {}'.format(key, value) for key, value in report_dict.items()])
        message += '\nPerformance report:\n' + report_str
    return message


@run_async
def send_async(bot, *args, **kwargs):
    """Send a message asynchronously"""
    if 'timeout' not in kwargs:
        kwargs['timeout'] = TIMEOUT

    try:
        bot.sendMessage(*args, **kwargs)
    except Exception as e:
        logging.exception(e)
