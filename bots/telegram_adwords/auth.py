import settings
from googleads import adwords, oauth2
from oauth2client import client


def get_oauth_uri():
    flow = client.OAuth2WebServerFlow(
        client_id=settings.OAUTH_CLIENT_ID,
        client_secret=settings.OAUTH_CLIENT_SECRET,
        scope=oauth2.GetAPIScope('adwords'),
        user_agent='Test',
        redirect_uri=settings.OAUTH_REDIRECT_URI)
    flow.params['access_type'] = 'offline'
    flow.params['prompt'] = 'consent'

    auth_uri = flow.step1_get_authorize_url()
    return auth_uri


def setup_client(access_token, refresh_token=None, token_expiry=None, client_customer_id=None):
    if access_token and refresh_token:
        oauth2_client = oauth2.GoogleRefreshTokenClient(
            client_id=settings.OAUTH_CLIENT_ID, client_secret=settings.OAUTH_CLIENT_SECRET,
            refresh_token=refresh_token, access_token=access_token)
    elif access_token and token_expiry:
        oauth2_client = oauth2.GoogleAccessTokenClient(
            token_expiry=token_expiry, access_token=access_token)
    else:
        raise Exception("Invalid OAuth credentials")

    adwords_client = adwords.AdWordsClient(
        settings.ADWORDS_DEVELOPER_TOKEN, oauth2_client)
    if client_customer_id:
        adwords_client.client_customer_id = client_customer_id
    return adwords_client


def setup_new_client(auth_code):
    flow = client.OAuth2WebServerFlow(
        client_id=settings.OAUTH_CLIENT_ID,
        client_secret=settings.OAUTH_CLIENT_SECRET,
        scope=oauth2.GetAPIScope('adwords'),
        user_agent='Test',
        redirect_uri=settings.OAUTH_REDIRECT_URI)
    # flow.params['access_type'] = 'offline'
    flow.params['prompt'] = 'consent'
    credentials = flow.step2_exchange(auth_code)
    # Store/update credentials
    adwords_client = setup_client(access_token=credentials.access_token,
                                  refresh_token=credentials.refresh_token,
                                  token_expiry=credentials.token_expiry)
    customer_service = adwords_client.GetService(
        'CustomerService', version='v201708')
    customers = customer_service.getCustomers()
    customer = customers[0] if customers else None
    adwords_client.client_customer_id = customer.customerId
    return adwords_client
