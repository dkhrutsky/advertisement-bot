import datetime
import logging


def get_campaigns(adwords_client):
    page_size = 100
    campaign_service = adwords_client.GetService('CampaignService', version='v201708')

    # Construct selector and get all campaigns.
    offset = 0
    selector = {
        'fields': ['Id', 'Name', 'Status'],
        'paging': {
            'startIndex': str(offset),
            'numberResults': str(page_size)
        }
    }

    more_pages = True
    campaigns = []
    while more_pages:
        page = campaign_service.get(selector)
        if 'entries' in page:
            campaigns.extend(page['entries'])
        offset += page_size
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])
    return campaigns


def get_campaign_details(adwords_client, campaign_id):
    campaign_service = adwords_client.GetService('CampaignService', version='v201708')
    campaign = None

    selector = {
        'fields': ['Id', 'Name', 'Status', 'ServingStatus', 'StartDate',
                   'EndDate', 'AdServingOptimizationStatus', 'Settings', 'Labels'],
        'paging': {
            'startIndex': str(0),
            'numberResults': str(1)
        },
        'predicates': {
            'field': 'Id',
            'operator': 'EQUALS',
            'values': campaign_id
        }
    }
    page = campaign_service.get(selector)
    if 'entries' in page:
        campaign = page['entries'][0]
    return campaign


def get_report(adwords_client, report_type='CAMPAIGN_PERFORMANCE_REPORT', date_range='LAST_WEEK', campaign_id=None):
    report_downloader = adwords_client.GetReportDownloader(version='v201708')
    report = {
        'reportName': 'Test campaign performance',
        'dateRangeType': date_range,
        'reportType': '{}'.format(report_type),
        'downloadFormat': 'CSV',
        'selector': {
            'fields': ['CampaignId', 'ImpressionReach', 'Impressions', 'Clicks', 'Cost',
                       'AveragePosition', 'AverageCpm', 'AverageCpc', 'Ctr', 'AllConversionValue']
        }
    }
    if campaign_id:
        report['selector']['predicates'] = {
            'field': 'CampaignId',
            'operator': 'EQUALS',
            'values': campaign_id
        }
    report_str = report_downloader.DownloadReportAsString(
        report, skip_report_header=True, skip_column_header=False,
        skip_report_summary=True, include_zero_impressions=True)
    return report_str


def get_report_by_day(adwords_client, report_type='CRITERIA_PERFORMANCE_REPORT', days=1):
    today = datetime.date.today()
    total_report = {}
    report_downloader = adwords_client.GetReportDownloader(version='v201708')
    for i in range(int(days) - 1, -1, -1):
        day = today - datetime.timedelta(days=i)
        day_str = day.strftime('%Y%m%d')
        report = {
            'reportName': '{} {}'.format(day_str, report_type),
            'dateRangeType': 'CUSTOM_DATE',
            'reportType': '{}'.format(report_type),
            'downloadFormat': 'CSV',
            'selector': {
                'dateRange': {'min': day_str, 'max': day_str},
                'fields': ['Id', 'CampaignId', 'AdGroupId', 'CriteriaType',
                           'Criteria', 'FinalUrls', 'Impressions', 'Clicks', 'Cost']
            }
        }
        report_str = report_downloader.DownloadReportAsString(
            report, skip_report_header=True, skip_column_header=False,
            skip_report_summary=True, include_zero_impressions=True)
        # Consider using 2 lists instead of dict
        total_report[day_str] = report_str
    return total_report


def campaign_action(adwords_client, campaign_id, action='SET', status='PAUSED'):
    campaign_service = adwords_client.GetService('CampaignService', version='v201708')
    operations = [{
        'operator': action,
        'operand': {
            'id': campaign_id,
            'status': status,
        }
    }]
    campaigns = campaign_service.mutate(operations)
    for campaign in campaigns:
        logging.info('Updated campaign {}: action {}, status {}'.format(
            campaign_id, action, status))
    return campaign
