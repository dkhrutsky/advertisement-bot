import psycopg2
import psycopg2.extras
import logging
import urllib
import settings


class SessionNotFoundError(Exception):
    pass


def parse_db_uri(db_uri):
    result = urllib.parse.urlparse(db_uri)
    username = result.username
    password = result.password
    database = result.path[1:]
    hostname = result.hostname
    return {"user": username, "password": password,
            "dbname": database, "host": hostname}


def setup_tables():
    with conn.cursor() as cur:
        cur.execute("CREATE EXTENSION IF NOT EXISTS pgcrypto")  # For gen_random_uuid()
        cur.execute("""
            CREATE TABLE IF NOT EXISTS sessions (
            id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
            user_id INTEGER,
            client_customer_id VARCHAR(20),
            access_token VARCHAR(200),
            token_expiry TIMESTAMP,
            refresh_token VARCHAR(200),
            is_active BOOL DEFAULT true)
        """)
    conn.commit()


def find_session_for_user(user_id):
    with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
        cur.execute("""
            SELECT user_id, access_token, token_expiry, refresh_token, client_customer_id
            FROM sessions
            WHERE user_id=%s AND is_active=true
        """, (user_id, ))
        session = cur.fetchone()
        if not session:
            raise SessionNotFoundError("No active session found for this user")
    return session


def set_session(user_id, oauth_client, client_customer_id):
    with conn.cursor() as cur:
        cur.execute("DELETE FROM sessions WHERE user_id=%s", (user_id, ))
        cur.execute("""
            INSERT INTO sessions (user_id, access_token, token_expiry, refresh_token, client_customer_id) VALUES
            (%s, %s, %s, %s, %s)
        """, (user_id, oauth_client.access_token, oauth_client.token_expiry, oauth_client.refresh_token, client_customer_id))
    conn.commit()


def delete_session(uuid):
    with conn.cursor() as cur:
        cur.execute("DELETE FROM sessions WHERE user_id=%s", (uuid, ))
    conn.commit()


logging.basicConfig(level=logging.DEBUG)
db_credentials = parse_db_uri(settings.DB_URI)
logging.debug('Connecting to database...')
conn = psycopg2.connect(**db_credentials)
logging.debug('Connection to database established')
setup_tables()
