import uuid
import logging
from shared import updater

USERS_JOBS = {}
job_queue = updater.job_queue


def add_notification(user_id, chat_id, callback, adwords_client, interval):
    job_uuid = str(uuid.uuid4())
    job = job_queue.run_repeating(callback, interval, name=job_uuid, context=[adwords_client, chat_id])
    try:
        u_jobs = USERS_JOBS[user_id]
    except KeyError:
        USERS_JOBS[user_id] = []
        u_jobs = USERS_JOBS[user_id]
    u_jobs.append(job_uuid)
    return job


def remove_notification(job_uuid):
    for job in filter(lambda x: x.name == job_uuid, job_queue.jobs()):
        logging.debug('Job {} (UUID: {}) is scheduled for removal'.format(job, job.name))
        for user_id, user_job_ids in USERS_JOBS.items():
            if job_uuid in user_job_ids:
                user_job_ids.remove(job_uuid)
        job.schedule_removal()


def list_notification_subscriptions(user_id):
    user_jobs_uuids = USERS_JOBS.get(user_id, [])
    if not len(user_jobs_uuids):
        logging.warning('No jobs for specified user')
    user_jobs = [job for job in job_queue.jobs() if job.name in user_jobs_uuids]
    # uuid, interval, type
    return [[job.name, job.callback.__name__, job._interval] for job in user_jobs]
