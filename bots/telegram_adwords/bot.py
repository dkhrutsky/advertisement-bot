from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ChatAction, ParseMode
from auth import setup_new_client, setup_client, get_oauth_uri
from utils import build_menu, format_campaign_message
from shared import updater
import adwords_queries as awq
import base64
import db
import logging
import os
import settings
import urllib
import plots
import notifications

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_session(user_id):
    session = db.find_session_for_user(user_id)
    adwords_client = setup_client(session['access_token'], session['refresh_token'],
                                  session['token_expiry'], session['client_customer_id'])
    return adwords_client


def start(bot, update, args):
    # Get auth code if presented, store credentials, init client
    if args:
        logger.debug(args)
        code = urllib.parse.unquote(''.join(args))
        # TODO: Validate auth code
        decoded = base64.urlsafe_b64decode(code).decode()
        logger.debug(decoded)
        adwords_client = setup_new_client(decoded)
        update.message.reply_text('Hello, {}'.format(adwords_client.client_customer_id))
        db.set_session(update.message.from_user.id, adwords_client.oauth2_client.oauth2credentials, adwords_client.client_customer_id)
        logger.debug('Session for user {} has been set'.format(update.message.from_user.id))
        main_menu(bot, update)
    else:
        try:
            session = db.find_session_for_user(update.message.from_user.id)
            update.message.reply_text('Hello, {}. Send your commands.'.format(session['client_customer_id']))
            main_menu(bot, update)
        except db.SessionNotFoundError:
            auth_uri = get_oauth_uri()
            update.message.reply_text("Please, authorize this bot to access your AdWords account by following this link:\n\n{}\n\n"
                                      "Then click the \"Start\" button when it appears.".format(auth_uri))


def campaigns(bot, update):
    try:
        if update.message:
            user = update.message.from_user
            chat = update.message.chat
        elif update.callback_query:
            user = update.callback_query.from_user
            chat = update.callback_query.message.chat
        adwords_client = get_session(user.id)
        campaigns = awq.get_campaigns(adwords_client)
        campaigns_str = "ID\tNAME\tSTATUS\n"
        campaigns_str += '\n'.join(["{}\t{}\t{}".format(c['id'], c['name'], c['status']) for c in campaigns])
        button_list = [InlineKeyboardButton(c['name'], callback_data='campaign/{}'.format(c['id'])) for c in campaigns]
        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
        bot.send_message(chat.id, "{} campaigns\n\n{}".format(len(campaigns), campaigns_str),
                         reply_markup=reply_markup)
    except db.SessionNotFoundError:
        auth_uri = get_oauth_uri()
        bot.send_message(chat.id, "Please, authorize this bot to access your AdWords account by clicking this link:\n\n{}".format(auth_uri))
        # TODO: Repeat command after authorization


def campaign_details(bot, update):
    campaign_id = update.callback_query.data[len('campaign/'):]
    adwords_client = get_session(update.callback_query.from_user.id)
    campaign = awq.get_campaign_details(adwords_client, campaign_id)
    if campaign:
        report = awq.get_report(adwords_client, campaign_id=campaign_id)
        message = format_campaign_message(campaign, report)
        new_status = 'Pause' if campaign.status == 'ENABLED' else 'Enable'
        buttons = [InlineKeyboardButton(new_status, callback_data='campaign/{}/{}'.format(new_status.lower(), campaign_id)),
                   InlineKeyboardButton('Remove', callback_data='campaign/remove/{}'.format(campaign_id))]
    else:
        message = 'No details about campaign {}'.format(campaign_id)
        buttons = []
    reply_markup = InlineKeyboardMarkup(build_menu(
        buttons=buttons,
        footer_buttons=[
            InlineKeyboardButton('Back', callback_data='campaigns'),
            InlineKeyboardButton('Menu', callback_data='menu'),
        ], n_cols=2))
    bot.send_message(update.callback_query.message.chat.id, message,
                     reply_markup=reply_markup, parse_mode=ParseMode.MARKDOWN)


def campaign_action(bot, update):
    command, campaign_id = update.callback_query.data.split('/')[1:]
    # Comands: pause, remove, enable
    adwords_client = get_session(update.callback_query.from_user.id)
    awq.campaign_action(adwords_client, campaign_id, 'SET', command.upper() + 'D')
    bot.send_message(update.callback_query.message.chat.id,
                     'Action \"{}\" has been applied to campaign {}'.format(command, campaign_id))


def campaigns_cb(bot, job):
    adwords_client = job.context[0]
    chat_id = job.context[1]
    campaigns = awq.get_campaigns(adwords_client)
    campaigns_str = "ID\tNAME\tSTATUS\n"
    campaigns_str += '\n'.join(["{}\t{}\t{}".format(c['id'], c['name'], c['status']) for c in campaigns])
    bot.send_message(chat_id, "{} campaigns\n\n{}".format(len(campaigns), campaigns_str))


def criteria_performance_report(bot, update, args):
    try:
        adwords_client = get_session(update.message.from_user.id)
        total_report = awq.get_report_by_day(adwords_client, 'CRITERIA_PERFORMANCE_REPORT', 7)

        for date_str, report_str in total_report.items():
            update.message.reply_text(report_str)
    except db.SessionNotFoundError:
        auth_uri = get_oauth_uri()
        update.message.reply_text("Please, authorize this bot to access your AdWords account by clicking this link:\n\n{}".format(auth_uri))


def criteria_performance_graph(bot, update):
    try:
        if update.message:
            user = update.message.from_user
            chat = update.message.chat
        elif update.callback_query:
            user = update.callback_query.from_user
            chat = update.callback_query.message.chat
        days = update.callback_query.data[len('reports/'):] if update.callback_query else 7
        adwords_client = get_session(user.id)
        bot.send_chat_action(chat_id=chat.id, action=ChatAction.TYPING)
        total_report = awq.get_report_by_day(adwords_client, 'CRITERIA_PERFORMANCE_REPORT', days)

        with open('test.json', 'w') as f:
            import json
            f.write(json.dumps(total_report))

        plot = plots.get_plot(total_report)
        bot.send_photo(chat.id, plot)
    except db.SessionNotFoundError:
        auth_uri = get_oauth_uri()
        bot.send_message(chat.id, "Please, authorize this bot to access your AdWords account by clicking this link:\n\n{}".format(auth_uri))


def ask_days_for_report(bot, update):
    button_list = [InlineKeyboardButton(str(days), callback_data='reports/' + str(days)) for days in (1, 2, 7, 10)]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(update.callback_query.message.chat.id, "Select days for report", reply_markup=reply_markup)


def stop(bot, update):
    db.delete_session(update.message.from_user.id)
    update.message.reply_text("Goodbye!")


def error(bot, update, error):
    logger.error('Update "{}" caused error "{}"'.format(update, error))


def main_menu(bot, update):
    chat = update.message.chat if update.message else update.callback_query.message.chat
    button_list = [InlineKeyboardButton("Campaigns", callback_data='campaigns'),
                   InlineKeyboardButton("Reports", callback_data='reports')]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(chat.id, "What do you want to do?", reply_markup=reply_markup)


def list_notifications(bot, update):
    jobs = notifications.list_notification_subscriptions(update.message.from_user.id)
    response = '\n'.join((''.join(str(job)) for job in jobs))
    button_list = [InlineKeyboardButton(job[0], callback_data='stopmsg/{}'.format(job[0])) for job in jobs]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
    if jobs:
        bot.send_message(update.message.chat.id, "Your notifications:\n{}".format(response),
                         reply_markup=reply_markup)
    else:
        bot.send_message(update.message.chat.id, "You don't have notifications.")


def add_notification(bot, update, args):
    interval = int(args[0]) if len(args) else 10
    adwords_client = get_session(update.message.from_user.id)
    notifications.add_notification(update.message.from_user.id, update.message.chat.id,
                                   campaigns_cb, adwords_client, interval)
    bot.send_message(update.message.chat.id, "Notifying every {} seconds".format(interval))


def remove_notification(bot, update):
    job_uuid = update.callback_query.data[len('stopmsg/'):]
    notifications.remove_notification(job_uuid)
    bot.send_message(update.callback_query.message.chat.id, "Notification removed")


if __name__ == '__main__':
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start, pass_args=True))
    dp.add_handler(CommandHandler('stop', stop))
    dp.add_handler(CommandHandler('menu', main_menu))
    dp.add_handler(CommandHandler('performance', criteria_performance_report, pass_args=True))
    dp.add_handler(CommandHandler('perfgraph', criteria_performance_graph, pass_args=True))
    dp.add_handler(CommandHandler('campaigns', campaigns))
    dp.add_handler(CommandHandler('notify', add_notification, pass_args=True))
    dp.add_handler(CommandHandler('notifications', list_notifications))

    dp.add_handler(CallbackQueryHandler(campaign_details, pattern=r'^campaign/(\d-?)+'))
    dp.add_handler(CallbackQueryHandler(campaign_action, pattern=r'^campaign/(enable|remove|pause)/(\d-?)+'))
    dp.add_handler(CallbackQueryHandler(campaigns, pattern=r'^campaigns$'))
    dp.add_handler(CallbackQueryHandler(main_menu, pattern=r'^menu$'))
    dp.add_handler(CallbackQueryHandler(ask_days_for_report, pattern=r'^reports$'))
    dp.add_handler(CallbackQueryHandler(criteria_performance_graph, pattern=r'^reports/\d+$'))
    dp.add_handler(CallbackQueryHandler(remove_notification, pattern=r'^stopmsg/(\w-?)+$'))

    dp.add_error_handler(error)

    if os.environ.get('USE_WEBHOOK'):
        logger.info('Starting bot using webhook')
        updater.start_webhook(
            listen='0.0.0.0',
            port=8443,
            url_path=settings.BOT_TOKEN,
            key='private.key',
            cert='cert.pem',
            webhook_url=settings.WEBHOOK_URL + settings.BOT_TOKEN)
    else:
        logger.info('Starting bot using polling')
        updater.start_polling()
    updater.idle()
    logger.info('Closing DB connection')
    db.conn.close()
